from peewee import MySQLDatabase
import config

_db = MySQLDatabase(config.database['database'])


def init():
    _db.init(**config.database)


def get():
    return _db


def close():
    if not _db.is_closed():
        _db.close()
