import config
import util
from app import app
import models
import controllers

if config.env == 'heroku':
    application = app
else:
    app.run(debug=True)
