from inspect import isclass


class ExceptionExt(Exception):
    code = 0

    @classmethod
    def get_code(cls):
        return cls.code

    def __int__(self):
        return type(self).code


class GenericError(ExceptionExt):
    code = 0


class ParameterError(ExceptionExt):
    code = 1


class EntityDoesNotExistError(ExceptionExt):
    code = 2


class EntityAlreadyExistError(ExceptionExt):
    code = 3


class EntityCreationFailed(ExceptionExt):
    code = 4


class InvalidOperationError(ExceptionExt):
    code = 5


class InvalidValueError(ExceptionExt):
    code = 6


class AuthenticationError(ExceptionExt):
    code = 100


class ExpiredTokenError(ExceptionExt):
    code = 101


class InvalidTokenError(ExceptionExt):
    code = 102


class MissingTokenError(ExceptionExt):
    code = 103


class UserInactiveError(ExceptionExt):
    code = 104


class AccessLevelLowError(ExceptionExt):
    code = 105


def get_error_dict():
    return {name: attr.get_code()
            for name, attr in globals().items()
            if isclass(attr) and issubclass(attr, ExceptionExt) and attr is not ExceptionExt}
