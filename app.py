from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS
from os.path import basename
import config
import util
import traceback
import sys
import re
import database
from models import *


app = Flask(__name__)
app.debug = True

# Enable cross-domain requests
CORS(app, origins=config.allowed_origins)


@app.route('/favicon.ico')
def serve_favicon():
    return send_from_directory(util.root_dir(), 'favicon.ico')


@app.before_request
def init_database_conn():
    database.init()


@app.after_request
def close_database_conn(response_class):
    database.close()
    return response_class


def ok(**data):
    return jsonify(dict(
        success=True,
        data=json_safe_data(data)
    ))


def fail(message=None, code=None, data=None, error=None):
    if isinstance(code, ExceptionExt):
        cd = code.get_code()
    else:
        cd = util.safe_cast(code, int, 0)

    if error:
        message = message if message else str(error)
        if isinstance(error, ExceptionExt):
            cd = error.get_code()

    message = message if message else "Unknown error"

    return jsonify(dict(
        success=False,
        message=message,
        code=cd,
        data=json_safe_data(data)
    ))


def require_args(*tuples):
    validated = []
    for name, itstype in tuples:
        if name not in request.values:
            raise ParameterError("Argument '%s' is missing" % name)
        try:
            validated.append(itstype(request.values.get(name)))
        except (ValueError, TypeError):
            raise ParameterError("Argument '%s' has wrong value" % name)
    return tuple(validated)


def maybe_args(*tuples):
    validated = []
    for name, itstype, default in tuples:
        if name not in request.values:
            validated.append(default)
            continue
        try:
            validated.append(itstype(request.values.get(name, default)))
        except (ValueError, TypeError):
            raise ParameterError("Argument '%s' has wrong value" % name)
    return tuple(validated)


class Auth:
    _user = None

    def __init__(self, user_id=None, access_level=0):
        self._user_id = user_id
        self._access_level = access_level

    def user_id(self):
        return self.user_id

    def has_access(self, level):
        return self._access_level >= User.access_level_code(level)

    def get_user(self):
        if not Auth._user:
            Auth._user = User.get(id=self.user_id)
        return Auth._user

    @classmethod
    def generate_auth_token(cls, user):
        payload = {
            'exp': datetime.utcnow() + timedelta(**config.token_expiration_delta),
            'iat': datetime.utcnow(),
            'user': user.id,
            'access': user.access_level
        }
        return jwt.encode(
            payload,
            config.auth_secret_key,
            algorithm='HS256',
            headers={'user': user.id}
        ).decode('utf-8')

    @classmethod
    def authenticate(cls, token):
        try:
            payload = jwt.decode(token, config.auth_secret_key)
            return Auth(payload['user'], payload['access'])
        except jwt.ExpiredSignatureError:
            raise ExpiredTokenError("Expired token")
        except jwt.InvalidTokenError:
            raise InvalidTokenError("Invalid token")

    @classmethod
    def log_in(cls, email: str, password: str):
        try:
            user = User.get(email=email)
            if user.password == password:
                return user, cls.generate_auth_token(user)
            else:
                raise AuthenticationError('Wrong password')
        except User.DoesNotExist:
            raise EntityDoesNotExistError("User doesn't exist")


class RestService:
    """
    Class used to automatically gather info about endpoints
    """

    def __init__(self):
        self.bundles = dict()
        self.param_regexp = re.compile(r'<\w+:([\w_]+)>')
        self._auth = None

    def __str__(self):
        return str(self.bundles)

    def get_schema(self):
        data = dict()
        for bname, bundle in self.bundles.items():
            data[bname] = dict()
            for aname, action in bundle.items():
                data[bname][aname] = dict(
                    endpoint=request.url_root + action['endpoint'][1:],
                    method=action['method']
                )
        return data

    def get_complete_map(self):
        return self.bundles

    def auth(self):
        return self._auth

    def load_controller(self, func, access_level=1):
        def controller_wrapper(*pargs, **kwargs):
            try:
                if not access_level:
                    return func(*pargs, **kwargs)
                else:
                    if 'auth_token' not in request.values:
                        return fail("auth_token is missing", MissingTokenError)
                    token = request.values['auth_token']
                    self._auth = Auth.authenticate(token)
                    if self._auth.has_access(access_level):
                        return func(*pargs, **kwargs)
                    else:
                        return fail('Access level "%s" is too low' % access_level, AccessLevelLowError)
            except:
                # Intercept all uncaught exceptions within controllers
                if config.debug:
                    ex_type, ex, tb = sys.exc_info()
                    if isinstance(ex, ExceptionExt):
                        return fail(error=ex, data=traceback.format_tb(tb))
                    else:
                        return fail(str(ex), data=traceback.format_tb(tb))
                else:
                    return fail()
        return controller_wrapper

    def route(self, path, method='GET', access_level=None, arglist=None, **options):

        if 'methods' in options:
            del options['methods']

        arglist = arglist or []

        # Set normal access level by default
        access_level = access_level if access_level is not None else 1

        def wrapper(func):

            # Get module name, where function is defined
            # Cannot use func.__module__ since running under CGI
            mod = basename(traceback.extract_stack()[-2].filename)[:-3]

            # Manually register rule
            app.add_url_rule(
                rule=path,
                # Need to prefix endpoint to avoid using unique names across all modules
                endpoint='%s_%s' % (mod, func.__name__),
                # Manually decorating function with access level validator
                view_func=self.load_controller(func, access_level),
                methods=[method],
                **options
            )

            bundle_name = str(mod)
            info = func.__doc__ if func.__doc__ is not None else ''

            if bundle_name not in self.bundles:
                self.bundles[bundle_name] = dict()

            if access_level:
                arglist.append('auth_token')

            self.bundles[bundle_name][func.__name__] = dict(
                endpoint=self.param_regexp.sub(r'%\1%', path),
                method=method,
                info=info.strip(),
                args=arglist,
                access_level=User.access_level_name(access_level)
            )

        return wrapper

    def get(self, *pargs, **kwargs):
        return self.route(*pargs, method='GET', **kwargs)

    def post(self, *pargs, **kwargs):
        return self.route(*pargs, method='POST', **kwargs)

    def put(self, *pargs, **kwargs):
        return self.route(*pargs, method='PUT', **kwargs)

    def delete(self, *pargs, **kwargs):
        return self.route(*pargs, method='DELETE', **kwargs)


rest = RestService()
