import re
import config
import os


def safe_cast(value, desired_type, default=None):
    try:
        return desired_type(value)
    except (ValueError, TypeError):
        return default


def test_date(string: str):
    return test_date.rule.match(string)

test_date.rule = re.compile(r'^\d{4}-\d{2}-\d{2}$')


def root_dir():
    return os.path.dirname(os.path.abspath(__file__))


def rel_path(path: str):
    return root_dir() + path
