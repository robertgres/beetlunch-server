Endpoints
#########

Cdishes
=======

.. js:attribute:: cdishes.get_list
    
    Get list of compound dishes
    
    Endpoint
        ``/cdishes``
    
    Access level 
        ``normal``
    
    Arguments
        * limit
        * offset
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: cdishes.get
    
    Retrieves single compound dish with given ``cdish_id``
    
    Endpoint
        ``/cdishes/%cdish_id%``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: cdishes.create
    
    Creates new compound dish.
    On success returns new compound dish' data with nested dishes
    
    Endpoint
        ``/cdishes/create``
    
    Access level 
        ``admin``
    
    Arguments
        * name
        * price
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: cdishes.schedule
    
    Schedule compound dish for order on specific date.
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``
    
    Endpoint
        ``/cdishes/%cdish_id%/schedule``
    
    Access level 
        ``admin``
    
    Arguments
        * date
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: cdishes.unschedule
    
    UnSchedule given compound dish, which was previously scheduled for specific date
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``.
    Doesn't return error if dish isn't scheduled on given date
    
    Endpoint
        ``/cdishes/%cdish_id%/unschedule``
    
    Access level 
        ``admin``
    
    Arguments
        * date
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: cdishes.modify
    
    Modify existing compound dish' basic parameters.
    On success returns dish with altered data
    
    Endpoint
        ``/cdishes/%cdish_id%/modify``
    
    Access level 
        ``admin``
    
    Arguments
        * name
        * price
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: cdishes.delete
    
    Deletes compound dish and all its schedules
    
    Endpoint
        ``/cdishes/%cdish_id%/delete``
    
    Access level 
        ``admin``
    
    Arguments
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: cdishes.get_dishes
    
    Get list of all dishes associated with given compound dish
    
    Endpoint
        ``/cdishes/%cdish_id%/dishes``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: cdishes.add_dishes
    
    Put one or more dishes to compound dish.
    To add single dish, provide single id as ``ids`` argument, e.g. ``ids=4``.
    To add multiple dishes, provide ``ids`` argument with ids separated by comma, e.g. ``ids=4,2,143``.
    To assign dishes to groups, provide ``groups`` arguments, which maps them to ``ids``.
    Number of ids must be greater than number of groups
    
    Endpoint
        ``/cdishes/%cdish_id%/dishes/add``
    
    Access level 
        ``admin``
    
    Arguments
        * ids
        * groups
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: cdishes.remove_dishes
    
    Remove one or more dishes from compound dish.
    To remove single dish, provide single id as ``ids`` argument, e.g. ``ids=4``.
    To remove multiple dishes, provide ``ids`` argument with ids separated by comma, e.g. ``ids=4,2,143``
    
    Already removed dishes are ignored
    
    Endpoint
        ``/cdishes/%cdish_id%/dishes/remove``
    
    Access level 
        ``admin``
    
    Arguments
        * ids
        * auth_token
    
    
    Method 
        ``POST``
    

Dishes
======

.. js:attribute:: dishes.get_list
    
    Get list of dishes
    
    Endpoint
        ``/dishes``
    
    Access level 
        ``normal``
    
    Arguments
        * limit
        * offset
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: dishes.get
    
    Retrieves single dish with given ``dish_id``
    
    Endpoint
        ``/dishes/%dish_id%``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: dishes.create
    
    Creates new dish
    On success returns new dish' data
    
    Endpoint
        ``/dishes/create``
    
    Access level 
        ``admin``
    
    Arguments
        * name
        * price
        * weight
        * type
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.schedule
    
    Schedule dish on specific date
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``
    
    Endpoint
        ``/dishes/%dish_id%/schedule``
    
    Access level 
        ``admin``
    
    Arguments
        * date
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.unschedule
    
    UnSchedule given dish, which was previously scheduled for specific date.
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``.
    Doesn't return error if dish isn't scheduled on given date
    
    Endpoint
        ``/dishes/%dish_id%/unschedule``
    
    Access level 
        ``admin``
    
    Arguments
        * date
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.edit
    
    Edit existing dish' parameters.
    On success returns dish with altered data
    
    Endpoint
        ``/dishes/%dish_id%/edit``
    
    Access level 
        ``admin``
    
    Arguments
        * name
        * price
        * weight
        * type
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.delete
    
    Deletes dish and all its schedules
    
    Endpoint
        ``/dishes/%dish_id%/delete``
    
    Access level 
        ``admin``
    
    Arguments
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.type_get_list
    
    Get list of dish types
    
    Endpoint
        ``/dishes/types``
    
    Access level 
        ``normal``
    
    Arguments
        * limit
        * offset
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: dishes.type_get
    
    Get single dish type
    
    Endpoint
        ``/dishes/types/%type_id%``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: dishes.type_create
    
    Create new dish type
    
    Endpoint
        ``/dishes/types/create``
    
    Access level 
        ``admin``
    
    Arguments
        * name
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.type_edit
    
    Edit single dish type
    
    Endpoint
        ``/dishes/types/%type_id%/edit``
    
    Access level 
        ``admin``
    
    Arguments
        * name
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: dishes.type_delete
    
    Delete single dish type
    
    Endpoint
        ``/dishes/types/%type_id%/delete``
    
    Access level 
        ``admin``
    
    Arguments
        * auth_token
    
    
    Method 
        ``POST``
    

Schedule
========

.. js:attribute:: schedule.get_weekly
    
    Retrieve scheduled meals as range of weeks, either as relative to current week, or with absolute days.
    By default only current week is returned
    
    Range could be specified either as difference related to current week, or as range of dates.
    
    - Relative format
    `from=[+-int]` and `to=[+-int]` where current week is 0
    `from` must be lesser than `to`
    
    - Absolute format
    Day specifies the week it belongs to.
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    
    Endpoint
        ``/schedule/weekly``
    
    Access level 
        ``normal``
    
    Arguments
        * from
        * to
        * date_from
        * date_to
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: schedule.get_daily
    
    Retrieve meals as range of days.
    By default only current day is returned
    
    Range could be specified either as difference related to current day, or as range of dates.
    
    - Relative format
    `from=[+-int]` and `to=[+-int]` where current day is 0
    `from` must be lesser than `to`
    
    - Absolute format
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    
    Endpoint
        ``/schedule/daily``
    
    Access level 
        ``normal``
    
    Arguments
        * from
        * to
        * date_from
        * date_to
        * auth_token
    
    
    Method 
        ``GET``
    

Orders
======

.. js:attribute:: orders.get_list
    
    Get list of all orders. By default fetches list of 10 most recent orders
    Provide ``user_ids`` to return orders only for selected users. Ignored for non-admins
    Only owned orders are returned for non-admins
    
    Endpoint
        ``/orders``
    
    Access level 
        ``normal``
    
    Arguments
        * limit
        * offset
        * user_ids
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: orders.get_weekly
    
    Retrieve orders as range of weeks, either as relative to current week, or with absolute dates.
    By default only current week is returned
    Provide ``user_ids`` to return orders only for selected users. Ignored for non-admins
    Only owned orders are returned for non-admins
    
    Range could be specified either as difference related to current week, or as range of dates.
    Range is inclusive
    
    - Relative format
    `from=[+-int]` and `to=[+-int]` where current week is 0
    `from` must be lesser than `to`
    
    - Absolute format
    Day specifies the week it belongs to.
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    
    Endpoint
        ``/orders/weekly``
    
    Access level 
        ``normal``
    
    Arguments
        * from
        * to
        * date_from
        * date_to
        * user_ids
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: orders.get_daily
    
    Retrieve orders as range of days, either as relative to current day, or with absolute dates.
    By default only current day is returned
    Provide ``user_ids`` to return orders only for selected users. Ignored for non-admins
    Only owned orders are returned for non-admins
    
    Range could be specified either as difference related to current day, or as range of dates.
    Range is inclusive
    
    - Relative format
    `from=[+-int]` and ``to=[+-int]` where current day is 0
    `from` must be lesser than `to`
    
    - Absolute format
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    
    Endpoint
        ``/orders/daily``
    
    Access level 
        ``normal``
    
    Arguments
        * from
        * to
        * date_from
        * date_to
        * user_ids
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: orders.place
    
    Create order for user. Default is current user
    Set ``user_id`` to place order for another users. Only for admins
    ``date`` is required in format ``YYYY-MM-DD``
    ``dishes`` and ``cdishes`` are lists of dishes and compound dishes IDs, separated by comma
    
    Endpoint
        ``/orders/place``
    
    Access level 
        ``normal``
    
    Arguments
        * user_id
        * date
        * cdishes
        * dishes
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: orders.get
    
    Get order information.
    Users with access level below admin can only see their own orders
    
    Endpoint
        ``/orders/%order_id%``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: orders.modify
    
    Modify order details.
    Missing arguments are ignored, but if no arguments provided, will return failure message.
    To empty either cdishes or dishes, include them but pass nothing.
    Note that order details could be modified only while order is placed, but not processed and paid
    
    Endpoint
        ``/orders/%order_id%/modify``
    
    Access level 
        ``admin``
    
    Arguments
        * cdishes
        * dishes
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: orders.cancel
    
    Cancel order.
    Note that orders are cancellable only while they are placed, but not processed and paid
    
    Endpoint
        ``/orders/%order_id%/cancel``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``POST``
    

Restinfo
========

.. js:attribute:: restinfo.get_schema
    
    Retrieves essential REST service endpoints schema
    and all application data that is required on front end
    
    Endpoint
        ``/``
    
    Access level 
        ``none``
    
    Arguments
    
    
    Method 
        ``GET``
    

Users
=====

.. js:attribute:: users.register
    
    Creates user with provided credentials.
    Depending on server settings, performs reCAPTCHA V2 validation.
    Password must be hashed with SHA-256 algorithm.
    Returns user object on success. Access token should be requested separately via login endpoint
    
    Endpoint
        ``/users/register``
    
    Access level 
        ``none``
    
    Arguments
        * recaptcha_token
        * email
        * password
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.login
    
    Authorize user. Returns user object and **auth_token**, which is essential for making all types of requests
    
    Endpoint
        ``/users/auth/login``
    
    Access level 
        ``none``
    
    Arguments
        * email
        * password
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.get_list
    
    Get list of users
    
    Endpoint
        ``/users``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: users.get
    
    Get information about user with given **user_id**
    
    Endpoint
        ``/users/%user_id%``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``GET``
    

.. js:attribute:: users.deactivate
    
    Deactivates user with given **user_id**.
    Users with access level below admin can deactivate only their own account
    
    Endpoint
        ``/users/%user_id%/deactivate``
    
    Access level 
        ``normal``
    
    Arguments
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.modify
    
    Modify user user information
    Users with access level below admin can modify only their own profile
    
    Endpoint
        ``/users/%user_id%/modify``
    
    Access level 
        ``normal``
    
    Arguments
        * nickname
        * first_name
        * last_name
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.change_password
    
    Change password for user with given **user_id**.
    Password must be hashed with SHA-256 algorithm.
    Users with access level below admin can change only their own password
    
    Endpoint
        ``/users/%user_id%/change_password``
    
    Access level 
        ``normal``
    
    Arguments
        * new_password
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.change_email
    
    Change email for user with given **user_id**, preserving all connected info.
    **email** must be unique among users.
    Users with access level below admin can change only their own email
    
    Endpoint
        ``/users/%user_id%/change_email``
    
    Access level 
        ``normal``
    
    Arguments
        * new_email
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.create
    
    Do not confuse with ``/users/auth/register`` endpoint.
    This is admin-level endpoint for creating new users.
    Password must be hashed with SHA-256 algorithm
    
    Endpoint
        ``/users/create``
    
    Access level 
        ``admin``
    
    Arguments
        * email
        * password
        * auth_token
    
    
    Method 
        ``POST``
    

.. js:attribute:: users.change_access_level
    
    Change access level for user. Also reactivates user who was previously *deactivated*.
    Optionally set desired **access_level**, default is **1** (normal user)
    
    Endpoint
        ``/users/%user_id%/change_access_level``
    
    Access level 
        ``admin``
    
    Arguments
        * access_level
        * auth_token
    
    
    Method 
        ``POST``
    