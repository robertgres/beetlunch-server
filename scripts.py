from sys import argv
from models import ModelRegister
import app
import controllers
import util
import textwrap


def _cmd_generate_docs():
    """
    Generates docs from source code
    """
    print('generating endpoints.md...')
    schema = app.rest.get_complete_map()
    file = open(util.rel_path('/docs/endpoints.rst'), 'w')

    output = 'Endpoints\n'
    output += '#'*(len(output) - 1)

    for bundle_name, bundle in schema.items():
        output += '\n\n%s' % bundle_name.capitalize()
        output += '\n' + '='*len(bundle_name)
        for action_name, action in bundle.items():
            output += '\n\n.. js:attribute:: %s.%s\n' % (bundle_name, action_name)

            arguments = ''
            for arg in action['args']:
                arguments += ' '*4 + textwrap.dedent("""
                * {arg}
                """.format(arg=arg).lstrip())

            body = textwrap.dedent("""
            {info}
            
            Endpoint
                ``{endpoint}``
            
            Access level 
                ``{access_level}``
            
            Arguments
            {arguments}
            
            Method 
                ``{method}``
            """).format(
                info='\n'.join([line.lstrip() for line in action['info'].split('\n')]),
                access_level=action['access_level'],
                endpoint=action['endpoint'],
                arguments=arguments,
                method=action['method'])

            output += '\n'.join(['    ' + line for line in body.split('\n')])

    file.write(output)
    print('...done!')


def _cmd_init_models(model_name=None):
    """
    Init database tables for single or all models
    usage: `init_models` or `init_models ModelName`
    """
    models = ModelRegister.get_models()
    if model_name:
        models = [m for m in models if m.__name__ == model_name]
    if not models:
        print('No models found')
    for model in models:
        print('Creating table for %s...' % model.__name__)
        try:
            model.create_table()
            print('...OK')
        except Exception as ex:
            print(str(ex))


def _cmd_renovate_model(model_name=None):
    """
    Try to drop and then create database table for single model
    Usage: `renovate_model ModelName`
    """
    if not model_name:
        print('Model name missing')
        return

    model = None

    for m in ModelRegister.get_models():
        if model_name == m.__name__:
            model = m

    if not model:
        print('Model %s not found' % model_name)
    else:
        confirm = input('table for %s will be dropped before being recreated, proceed? (y/n) ' % model_name) == 'y'
        if not confirm:
            print('Aborting')
            return
        print('Processing %s...' % model_name)
        try:
            print('Dropping table...')
            model.drop_table()
            print('...OK')
            print('Creating table...')
            model.create_table()
            print('...OK')
        except Exception as ex:
            print(str(ex))
            print('Maybe just drop that table manually...')

cmds = argv[1:]

available_commands = [c for c in globals().keys() if c.startswith('_cmd_')]


if not cmds:
    print('No command specified. Try "help" command to get list of available commands')
    exit()

requested_command = '_cmd_' + cmds[0]

if requested_command[5:] == 'help':
    print('Available commands:')
    for cmd in available_commands:
        print(' – %s' % cmd[5:])
        doc = globals()[cmd].__doc__
        if doc:
            print(doc)
elif requested_command in available_commands:
    attr = globals()[requested_command](*cmds[1:])
