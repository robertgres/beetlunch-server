from app import *


@rest.get('/schedule/weekly', arglist=['from', 'to', 'date_from', 'date_to'])
def get_weekly():
    """
    Retrieve scheduled meals as range of weeks, either as relative to current week, or with absolute days.
    By default only current week is returned

    Range could be specified either as difference related to current week, or as range of dates.

    - Relative format
    `from=[+-int]` and `to=[+-int]` where current week is 0
    `from` must be lesser than `to`

    - Absolute format
    Day specifies the week it belongs to.
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    """
    _from, _to, date_from, date_to = maybe_args(
        ('from', int, 0), ('to', int, 0), ('date_from', str, None), ('date_to', str, None))

    # Get week's monday
    df = date.today() + timedelta(weeks=_from) - timedelta(days=datetime.today().isoweekday()-1)
    # Get week's sunday
    dt = date.today() + timedelta(weeks=_to) + timedelta(days=7-datetime.today().isoweekday())

    if date_from:
        if not util.test_date(date_from):
            return fail("Invalid date format", ParameterError)
        df = datetime.strptime(date_from, "%Y-%m-%d").date()
        df = df - timedelta(days=df.isoweekday()-1)
    if date_to:
        if not util.test_date(date_to):
            return fail("Invalid date format", ParameterError)
        dt = datetime.strptime(date_to, "%Y-%m-%d").date()
        dt = dt + timedelta(days=7-dt.isoweekday())

    if dt < df:
        return fail("date_from or from must be lesser than date_to or to")

    # Calculate number of weeks in our diff
    nweeks = ((dt - df).days + 1) / 7
    if nweeks*7 > config.max_days_in_response:
        return fail("Too many days, maximum is %s" % config.max_days_in_response)

    scheduled_dishes = ScheduleDishes.fetch_range(df, dt)
    scheduled_cdishes = ScheduleCDishes.fetch_range(df, dt)

    # Accumulate orders in weekly list of days
    weeks = []
    for week in range(0, int(nweeks)):
        wstart = df + timedelta(weeks=week)
        wend = wstart + timedelta(days=6)
        days = []
        for day in range(7):
            ddate = wstart + timedelta(days=day)
            days.append(dict(
                date=ddate.strftime("%Y-%m-%d"),
                dishes=[link.subject for link in scheduled_dishes if link.date == ddate],
                cdishes=[link.subject for link in scheduled_cdishes if link.date == ddate],
            ))
        weeks.append(dict(
            start=wstart.strftime("%Y-%m-%d"),
            end=wend.strftime("%Y-%m-%d"),
            days=days
        ))

    return ok(weeks=weeks)


@rest.get('/schedule/daily', arglist=['from', 'to', 'date_from', 'date_to'])
def get_daily():
    """
    Retrieve meals as range of days.
    By default only current day is returned

    Range could be specified either as difference related to current day, or as range of dates.

    - Relative format
    `from=[+-int]` and `to=[+-int]` where current day is 0
    `from` must be lesser than `to`

    - Absolute format
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    """
    _from, _to, date_from, date_to = maybe_args(
        ('from', int, 0), ('to', int, 0), ('date_from', str, None), ('date_to', str, None))

    df = date.today() + timedelta(days=_from)
    dt = date.today() + timedelta(days=_to)

    if date_from:
        if not util.test_date(date_from):
            return fail("Invalid date format", ParameterError)
        df = datetime.strptime(date_from, "%Y-%m-%d").date()
    if date_to:
        if not util.test_date(date_to):
            return fail("Invalid date format", ParameterError)
        dt = datetime.strptime(date_to, "%Y-%m-%d").date()

    if dt < df:
        return fail("date_from or from must be lesser than date_to or to")

    ndays = (dt - df).days + 1
    if ndays > config.max_days_in_response:
        return fail("Too many days, maximum is %s" % config.max_days_in_response)

    scheduled_dishes = ScheduleDishes.fetch_range(df, dt)
    scheduled_cdishes = ScheduleCDishes.fetch_range(df, dt)

    days = []
    for day in range(ndays):
        ddate = df + timedelta(days=day)
        days.append(dict(
            date=ddate.strftime("%Y-%m-%d"),
            dishes=[link.subject for link in scheduled_dishes if link.date == ddate],
            cdishes=[link.subject for link in scheduled_cdishes if link.date == ddate],
        ))

    return ok(days=days)
