from app import *
from itertools import zip_longest


@rest.get('/cdishes', arglist=['limit', 'offset'])
def get_list():
    """
    Get list of compound dishes
    """
    limit, offset = maybe_args(('limit', int, 10), ('offset', int, 0))
    cdishes = CDish.select().limit(limit).offset(offset)
    return ok(cdishes=list(cdishes))


@rest.get('/cdishes/<int:cdish_id>')
def get(cdish_id):
    """
    Retrieves single compound dish with given ``cdish_id``
    """
    return ok(cdish=CDish.handle_get(id=cdish_id))


@rest.post('/cdishes/create', access_level='admin', arglist=['name', 'price'])
def create():
    """
    Creates new compound dish.
    On success returns new compound dish' data with nested dishes
    """
    name, = require_args(('name', str))
    cdish = CDish.create(name=name)
    for name, value in request.values.items():
        if name == 'price':
            cdish.change_property(name, value)
    return ok(cdish=cdish)


@rest.post('/cdishes/<int:cdish_id>/schedule', access_level='admin', arglist=['date'])
def schedule(cdish_id):
    """
    Schedule compound dish for order on specific date.
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``
    """
    dt, = require_args(('date', str))
    return ok() if ScheduleCDishes.schedule(cdish_id, dt) else fail("Scheduling dish failed")


@rest.post('/cdishes/<int:cdish_id>/unschedule', access_level='admin', arglist=['date'])
def unschedule(cdish_id):
    """
    UnSchedule given compound dish, which was previously scheduled for specific date
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``.
    Doesn't return error if dish isn't scheduled on given date
    """
    dt, = require_args(('date', str))
    return ok() if ScheduleCDishes.unschedule(cdish_id, dt) else fail("Scheduling dish failed")


@rest.post('/cdishes/<int:cdish_id>/modify', access_level='admin', arglist=['name', 'price'])
def modify(cdish_id):
    """
    Modify existing compound dish' basic parameters.
    On success returns dish with altered data
    """
    cdish = CDish.handle_get(id=cdish_id)
    report = {name: cdish.change_property(name, value) for name, value in request.values.items()
              if name == 'name' or name == 'price'}
    return ok(cdish=cdish, change_report=report)


@rest.post('/cdishes/<int:cdish_id>/delete', access_level='admin')
def delete(cdish_id):
    """
    Deletes compound dish and all its schedules
    """
    cdish = CDish.handle_get(id=cdish_id)
    return ok() if cdish.delete_instance(recursive=True) else fail("Compound dish wan't deleted properly")


@rest.get('/cdishes/<int:cdish_id>/dishes')
def get_dishes(cdish_id):
    """
    Get list of all dishes associated with given compound dish
    """
    dishes = Dish.select().join(CDishDishes, on=CDishDishes.dish).where(CDishDishes.cdish == cdish_id)
    return ok(dishes=list(dishes))


@rest.post('/cdishes/<int:cdish_id>/dishes/add', arglist=['ids', 'groups'], access_level='admin')
def add_dishes(cdish_id):
    """
    Put one or more dishes to compound dish.
    To add single dish, provide single id as ``ids`` argument, e.g. ``ids=4``.
    To add multiple dishes, provide ``ids`` argument with ids separated by comma, e.g. ``ids=4,2,143``.
    To assign dishes to groups, provide ``groups`` argument, which maps them to ``ids``,
    such as ``ids=1,2,3&groups=soup,soup,beverage`` will associate dishes with ids **1** and **2** to group **soup**
    and dish with id **3** to group **beverage**
    """
    cdish = CDish.handle_get(id=cdish_id)

    idsstr, = require_args(('ids', str))
    groupsstr, = maybe_args(('groups', str, ''))
    ids = [int(id) for id in idsstr.split(',')]
    groups = [group for group in groupsstr.split(',')] if groupsstr else []

    if len(groups) > len(ids):
        del groups[len(ids):]
    mapping = zip_longest(ids, groups, fillvalue=0)

    for dish_id, group in mapping:
        CDishDishes.link(cdish.get_id(), dish_id, group)

    return ok(cdish=cdish)


@rest.post('/cdishes/<int:cdish_id>/dishes/remove', arglist=['ids'], access_level='admin')
def remove_dishes(cdish_id):
    """
    Remove one or more dishes from compound dish.
    To remove single dish, provide single id as ``ids`` argument, e.g. ``ids=4``.
    To remove multiple dishes, provide ``ids`` argument with ids separated by comma, e.g. ``ids=4,2,143``

    Already removed dishes are ignored
    """
    cdish = CDish.handle_get(id=cdish_id)

    idsstr, = require_args(('ids', str))
    ids = [int(id) for id in idsstr.split(',')]
    for dish_id in ids:
        CDishDishes.unlink(cdish, dish_id)

    return ok(cdish=CDish.get(id=cdish_id))
