from app import *


@rest.get('/dishes', arglist=['limit', 'offset'])
def get_list():
    """
    Get list of dishes
    """
    limit, offset = maybe_args(('limit', int, 10), ('offset', int, 0))
    dishes = Dish.select().limit(limit).offset(offset)
    return ok(dishes=list(dishes))


@rest.get('/dishes/<int:dish_id>')
def get(dish_id):
    """
    Retrieves single dish with given ``dish_id``
    """
    return ok(dish=Dish.handle_get(id=dish_id))


@rest.post('/dishes/create', access_level='admin', arglist=['name', 'price', 'weight', 'type'])
def create():
    """
    Creates new dish
    On success returns new dish' data
    """
    name, = require_args(('name', str))
    dish = Dish.create(name=name)
    for name, value in request.values.items():
        if name == 'price' or name == 'weight' or name == 'type':
            dish.change_property(name, value)
    return ok(dish=dish)


@rest.post('/dishes/<int:dish_id>/schedule', access_level='admin', arglist=['date'])
def schedule(dish_id):
    """
    Schedule dish on specific date
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``
    """
    dt, = require_args(('date', str))
    return ok() if ScheduleDishes.schedule(dish_id, dt) else fail("Scheduling dish failed")


@rest.post('/dishes/<int:dish_id>/unschedule', access_level='admin', arglist=['date'])
def unschedule(dish_id):
    """
    UnSchedule given dish, which was previously scheduled for specific date.
    date must be in format ``YYYY-MM-DD`` like ``2017-11-27``.
    Doesn't return error if dish isn't scheduled on given date
    """
    dt, = require_args(('date', str))
    return ok() if ScheduleDishes.unschedule(dish_id, dt) else fail("Scheduling dish failed")


@rest.post('/dishes/<int:dish_id>/edit', access_level='admin', arglist=['name', 'price', 'weight', 'type'])
def edit(dish_id):
    """
    Edit existing dish' parameters.
    On success returns dish with altered data
    """
    dish = Dish.handle_get(id=dish_id)
    report = {name: dish.change_property(name, value) for name, value in request.values.items()
              if name == 'name' or name == 'price' or name == 'weight' or name == 'type'}
    return ok(dish=dish, change_report=report)


@rest.post('/dishes/<int:dish_id>/delete', access_level='admin')
def delete(dish_id):
    """
    Deletes dish and all its schedules
    """
    dish = Dish.handle_get(id=dish_id)
    return ok() if dish.delete_instance(recursive=True) else fail("Dish wan't deleted properly")


@rest.get('/dishes/types', arglist=['limit', 'offset'])
def type_get_list():
    """
    Get list of dish types
    """
    limit, offset = maybe_args(('limit', int, 10), ('offset', int, 0))
    dtypes = DishType.select().limit(limit).offset(offset)
    return ok(types=list(dtypes))


@rest.get('/dishes/types/<int:type_id>')
def type_get(type_id):
    """
    Get single dish type
    """
    return ok(type=DishType.handle_get(id=type_id))


@rest.post('/dishes/types/create', access_level='admin', arglist=['name'])
def type_create():
    """
    Create new dish type
    """
    name, = require_args(('name', str))
    return ok(type=DishType.create_one(name=name))


@rest.post('/dishes/types/<int:type_id>/edit', access_level='admin', arglist=['name'])
def type_edit(type_id):
    """
    Edit single dish type
    """
    dtype = DishType.handle_get(id=type_id)
    report = {name: dtype.change_property(name, value) for name, value in request.values.items()
              if name == 'name'}
    return ok(type=dtype, change_report=report)


@rest.post('/dishes/types/<int:type_id>/delete', access_level='admin')
def type_delete(type_id):
    """
    Delete single dish type
    """
    dtype = DishType.handle_get(id=type_id)
    dishes = Dish.select().where(Dish.type == dtype)
    for dish in dishes:
        dish.type = None
        dish.save()
    success = dtype.delete_instance()
    return ok() if success else fail("Nothings seems to be deleted")
