from app import *


@rest.get('/', access_level=0)
def get_schema():
    """
    Retrieves essential REST service endpoints schema
    and all application data that is required on front end
    """
    appdata = dict(
        errors=get_error_dict(),
        recaptcha_public_key=config.recaptcha['public_key']
    )
    return ok(schema=rest.get_schema(), appdata=appdata)
