from app import *
import requests


@rest.post('/users/register', access_level=0, arglist=['recaptcha_token', 'email', 'password'])
def register():
    """
    Creates user with provided credentials.
    Depending on server settings, performs reCAPTCHA V2 validation.
    Password must be hashed with SHA-256 algorithm.
    Returns user object on success. Access token should be requested separately via login endpoint
    """
    rec_token = ''

    if config.recaptcha['enable']:
        rec_token, email, password = require_args(('recaptcha_token', str), ('email', str), ('password', str))
    else:
        email, password = require_args(('email', str), ('password', str))

    if config.recaptcha['enable']:
        validation_data = dict(
            secret=config.recaptcha['secret_key'],
            response=rec_token,
            remoteip=request.remote_addr
        )
        validation = requests.post(config.recaptcha['endpoint'], validation_data).json()
        if not validation['success']:
            return fail('reCAPTCHA validation failed (Are you human?)',
                        AuthenticationError, data=validation['error-codes'])

    user = User.register(email, password)
    return ok(user=user)


@rest.post('/users/auth/login', access_level=0, arglist=['email', 'password'])
def login():
    """
    Authorize user. Returns user object and **auth_token**, which is essential for making all types of requests
    """
    email, password = require_args(('email', str), ('password', str))
    user, auth_token = Auth.log_in(email, password)
    return ok(user=user, auth_token=auth_token)


@rest.get('/users')
def get_list():
    """
    Get list of users
    """
    try:
        limit = int(request.values.get('limit', 10))
        offset = int(request.values.get('offset', 0))
    except ValueError:
        return fail("Incorrect value", ParameterError)
    users = User.select().limit(limit).offset(offset)
    return ok(users=list(users))


@rest.get('/users/<int:user_id>')
def get(user_id):
    """
    Get information about user with given **user_id**
    """
    return ok(user=User.handle_get(id=user_id))


@rest.post('/users/<int:user_id>/deactivate')
def deactivate(user_id):
    """
    Deactivates user with given **user_id**.
    Users with access level below admin can deactivate only their own account
    """
    if user_id != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to deactivate another user", AccessLevelLowError)

    user = User.handle_get(id=user_id)
    return ok() if user.deactivate() else fail("Deactivation failed")


@rest.post('/users/<int:user_id>/modify', arglist=['nickname', 'first_name', 'last_name'])
def modify(user_id):
    """
    Modify user user information
    Users with access level below admin can modify only their own profile
    """
    if user_id != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to edit another user's profile", AccessLevelLowError)

    user = User.handle_get(id=user_id)
    report = {name: user.change_property(name, value) for name, value in request.values.items()
              if name == 'nickname' or name == 'first_name' or name == 'last_name'}
    return ok(user=user, change_report=report)


@rest.post('/users/<int:user_id>/change_password', arglist=['new_password'])
def change_password(user_id):
    """
    Change password for user with given **user_id**.
    Password must be hashed with SHA-256 algorithm.
    Users with access level below admin can change only their own password
    """
    if user_id != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to change another user's password", AccessLevelLowError)

    new_pass = require_args(('new_password', str))
    user = User.handle_get(id=user_id)
    return ok() if user.set_password(new_pass) else fail("Password change failed")


@rest.post('/users/<int:user_id>/change_email', arglist=['new_email'])
def change_email(authorized_user, user_id):
    """
    Change email for user with given **user_id**, preserving all connected info.
    **email** must be unique among users.
    Users with access level below admin can change only their own email
    """
    if user_id != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to change another user's email", AccessLevelLowError)

    new_email = require_args(('new_email', str))
    user = User.handle_get(id=user_id)
    success = user.set_email(new_email)
    return ok(user=user) if success else fail("Email change failed")


@rest.post('/users/create', access_level='admin', arglist=['email', 'password'])
def create():
    """
    Do not confuse with ``/users/auth/register`` endpoint.
    This is admin-level endpoint for creating new users.
    Password must be hashed with SHA-256 algorithm
    """
    email, password = require_args(('email', str), ('password', str))
    user = User.register(email, password)
    return ok(user=user)


@rest.post('/users/<int:user_id>/change_access_level', access_level='admin', arglist=['access_level'])
def change_access_level(user_id):
    """
    Change access level for user. Also reactivates user who was previously *deactivated*.
    Optionally set desired **access_level**, default is **1** (normal user)
    """
    access_level = request.values.get('access_level', 1)
    user = User.handle_get(id=user_id)
    return ok() if user.set_access_level(access_level) else fail("Access level change failed")
