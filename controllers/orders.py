from app import *


@rest.get('/orders', arglist=['limit', 'offset', 'user_ids'])
def get_list():
    """
    Get list of all orders. By default fetches list of 10 most recent orders
    Provide ``user_ids`` to return orders only for selected users. Ignored for non-admins
    Only owned orders are returned for non-admins
    """
    limit, offset, user_ids = maybe_args(
        ('limit', int, 10), ('offset', int, 0), ('user_ids', str, ''))
    uids = [int(id) for id in user_ids.split(',')] if user_ids else None

    # Ignore all other ids if not admin
    if not rest.auth().has_access('admin'):
        uids = [rest.auth().user_id()]

    return ok(orders=Order.fetch_list(limit, offset, uids))


@rest.get('/orders/weekly', arglist=['from', 'to', 'date_from', 'date_to', 'user_ids'])
def get_weekly():
    """
    Retrieve orders as range of weeks, either as relative to current week, or with absolute dates.
    By default only current week is returned
    Provide ``user_ids`` to return orders only for selected users. Ignored for non-admins
    Only owned orders are returned for non-admins

    Range could be specified either as difference related to current week, or as range of dates.
    Range is inclusive

    - Relative format
    `from=[+-int]` and `to=[+-int]` where current week is 0
    `from` must be lesser than `to`

    - Absolute format
    Day specifies the week it belongs to.
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    """
    _from, _to, date_from, date_to, user_ids = maybe_args(
        ('from', int, 0), ('to', int, 0), ('date_from', str, None), ('date_to', str, None), ('user_ids', str, None))
    uids = [int(id) for id in user_ids.split(',')] if user_ids else None

    # Ignore all other ids if not admin
    if not rest.auth().has_access('admin'):
        uids = [rest.auth().user_id()]

    # Get week's monday
    df = date.today() + timedelta(weeks=_from) - timedelta(days=datetime.today().isoweekday()-1)
    # Get week's sunday
    dt = date.today() + timedelta(weeks=_to) + timedelta(days=7-datetime.today().isoweekday())

    if date_from:
        if not util.test_date(date_from):
            return fail("Invalid date format", ParameterError)
        df = datetime.strptime(date_from, "%Y-%m-%d").date()
        df = df - timedelta(days=df.isoweekday()-1)
    if date_to:
        if not util.test_date(date_to):
            return fail("Invalid date format", ParameterError)
        dt = datetime.strptime(date_to, "%Y-%m-%d").date()
        dt = dt + timedelta(days=7-dt.isoweekday())

    if dt < df:
        return fail("date_from or from must be lesser than date_to or to")

    orders = Order.fetch_range(df, dt, uids)

    # Calculate number of weeks in our diff
    nweeks = ((dt - df).days + 1) / 7
    if nweeks*7 > config.max_days_in_response:
        return fail("Too many days, maximum is %s" % config.max_days_in_response)

    # Accumulate orders in weekly list of days
    weeks = []
    for week in range(0, int(nweeks)):
        wstart = df + timedelta(weeks=week)
        wend = wstart + timedelta(days=6)
        days = []
        for day in range(7):
            ddate = wstart + timedelta(days=day)
            days.append(dict(
                date=ddate.strftime("%Y-%m-%d"),
                orders=[order for order in orders if order.date == ddate]
            ))
        weeks.append(dict(
            start=wstart.strftime("%Y-%m-%d"),
            end=wend.strftime("%Y-%m-%d"),
            days=days
        ))

    return ok(weeks=weeks)


@rest.get('/orders/daily', arglist=['from', 'to', 'date_from', 'date_to', 'user_ids'])
def get_daily():
    """
    Retrieve orders as range of days, either as relative to current day, or with absolute dates.
    By default only current day is returned
    Provide ``user_ids`` to return orders only for selected users. Ignored for non-admins
    Only owned orders are returned for non-admins

    Range could be specified either as difference related to current day, or as range of dates.
    Range is inclusive

    - Relative format
    `from=[+-int]` and ``to=[+-int]` where current day is 0
    `from` must be lesser than `to`

    - Absolute format
    `date_from=[YYYY-MM-DD]` and `date_to=[YYYY-MM-DD]`
    `date_from` must be lesser than `date_to`
    """
    _from, _to, date_from, date_to, user_ids = maybe_args(
        ('from', int, 0), ('to', int, 0), ('date_from', str, None), ('date_to', str, None), ('user_ids', str, None))
    uids = [int(id) for id in user_ids.split(',')] if user_ids else None

    # Ignore all other ids if not admin
    if not rest.auth().has_access('admin'):
        uids = [rest.auth().user_id()]

    df = date.today() + timedelta(days=_from)
    dt = date.today() + timedelta(days=_to)

    if date_from:
        if not util.test_date(date_from):
            return fail("Invalid date format", ParameterError)
        df = datetime.strptime(date_from, "%Y-%m-%d").date()
    if date_to:
        if not util.test_date(date_to):
            return fail("Invalid date format", ParameterError)
        dt = datetime.strptime(date_to, "%Y-%m-%d").date()

    if dt < df:
        return fail("date_from or from must be lesser than date_to or to")

    ndays = (dt - df).days + 1
    if ndays > config.max_days_in_response:
        return fail("Too many days, maximum is %s" % config.max_days_in_response)

    orders = Order.fetch_range(df, dt, uids)

    days = []
    for day in range(ndays):
        ddate = df + timedelta(days=day)
        days.append(dict(
            date=ddate.strftime("%Y-%m-%d"),
            orders=[order for order in orders if order.date == ddate]
        ))

    return ok(days=days)


@rest.post('/orders/place', arglist=['user_id', 'date', 'cdishes', 'dishes'])
def place():
    """
    Create order for user. Default is current user
    Set ``user_id`` to place order for another users. Only for admins
    ``date`` is required in format ``YYYY-MM-DD``
    ``dishes`` and ``cdishes`` are lists of dishes and compound dishes IDs, separated by comma
    """
    for_date = require_args(('date', str))
    user_id, cdishes, dishes = maybe_args(('user_id', int, None), ('cdishes', str, ''), ('dishes', str, ''))

    uid = user_id if user_id else rest.auth().user_id()

    if uid != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to create orders for other users")

    if not re.match(r'^\d{4}-\d{2}-\d{2}$', for_date):
        return fail("Invalid date format", ParameterError)

    dt = datetime.strptime(for_date, "%Y-%m-%d")
    cdishes_ids = [int(id) for id in cdishes.split(',')] if cdishes else []
    dishes_ids = [int(id) for id in dishes.split(',')] if cdishes else []

    odata = OrderData.from_ids(dishes_ids, cdishes_ids)
    return ok(order=Order.place(uid, odata, dt))


@rest.get('/orders/<int:order_id>')
def get(order_id):
    """
    Get order information.
    Users with access level below admin can only see their own orders
    """
    order = Order.get(id=order_id)
    if order.user.get_id() != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to view this order", AccessLevelLowError)
    return ok(order=order)


@rest.post('/orders/<int:order_id>/modify', access_level='admin', arglist=['cdishes', 'dishes'])
def modify(order_id):
    """
    Modify order details.
    Missing arguments are ignored, but if no arguments provided, will return failure message.
    To empty either cdishes or dishes, include them but pass nothing.
    Note that order details could be modified only while order is placed, but not processed and paid
    """
    order = Order.get(id=order_id)
    if order.user.get_id() != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to modify this order", AccessLevelLowError)

    dishes, cdishes = maybe_args(('dishes', str, 'ignore'), ('cdishes', str, 'ignore'))
    if dishes == 'ignore' and cdishes == 'ignore':
        return fail('No changes provided', ParameterError)
    data = order.get_data()

    if dishes != 'ignore':
        dish_ids = map(int, dishes.split(','))
        data.dishes = list(Dish.select().where(Dish.id << dish_ids))
    if cdishes != 'ignore':
        cdish_ids = map(int, cdishes.split(','))
        data.cdishes = list(CDish.select().where(CDish.id << cdish_ids))

    return ok() if bool(order.modify(data)) else fail("Order modification failed")


@rest.post('/orders/<int:order_id>/cancel')
def cancel(order_id):
    """
    Cancel order.
    Note that orders are cancellable only while they are placed, but not processed and paid
    """
    order = Order.get(id=order_id)
    if order.user.get_id() != rest.auth().user_id() and not rest.auth().has_access('admin'):
        return fail("Not enough rights to modify this order", AccessLevelLowError)

    return ok() if order.cancel() else fail("Order cancellation failed")
