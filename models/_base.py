from errors import *
from decimal import Decimal
from peewee import *
from playhouse.shortcuts import model_to_dict
import util
from datetime import datetime, date, timedelta
import database
import re


def json_safe_data(data):
    def process_value(value):
        if isinstance(value, dict):
            return {key: process_value(val) for key, val in value.items()}
        elif isinstance(value, list):
            return [process_value(val) for val in value]
        elif hasattr(value, 'collect_data'):
            return value.collect_data()
        else:
            return value

    return process_value(data)


class ModelRegister(type(Model)):
    """
    This metaclass is used to solve problem with one-way model dependency,
    which allows to get model without importing it
    """
    _registered_models = []

    def __init__(self, name, bases, clsdict):
        super(ModelRegister, self).__init__(name, bases, clsdict)
        ModelRegister._registered_models.append(self)

    @classmethod
    def lookup_model(cls, name: str):
        for model in cls._registered_models:
            if model.__name__ == name:
                return model
        else:
            raise EntityDoesNotExistError("Model %s doesn't exist" % name)

    @classmethod
    def get_models(cls):
        return cls._registered_models


class BaseModel(Model, metaclass=ModelRegister):
    @classmethod
    def lookup_model(cls, name):
        return ModelRegister.lookup_model(name)

    @classmethod
    def handle_get(cls, *pargs, **kwargs):
        try:
            return cls.get(*pargs, **kwargs)
        except cls.DoesNotExist:
            raise EntityDoesNotExistError("%s with given parameters doesn't exist" % cls.__name__)

    def serialize(self):
        def process_dict(dt):
            for key in dt:
                tp = type(dt[key])
                if tp is date:
                    dt[key] = date.strftime(dt[key], '%Y-%m-%d')
                elif tp is datetime:
                    dt[key] = dt[key].isoformat()
                elif tp is Decimal:
                    dt[key] = float(dt[key])
                elif isinstance(dt[key], dict):
                    dt[key] = process_dict(dt[key])
            return dt

        return process_dict(dict(self))

    def collect_data(self):
        return self.serialize()

    def change_property(self, propname: str, new_value):
        setter = 'set_' + propname
        if not hasattr(self, setter):
            raise NotImplementedError("Method %s must be implemented" % setter)
        return getattr(self, setter)(new_value)

    def __iter__(self):
        d = model_to_dict(self)

        for key in model_to_dict(self).keys():
            yield key, d[key]

    class Meta:
        database = database.get()


class SchedulerModel(BaseModel):
    subject = NotImplemented
    date = DateField()

    @classmethod
    def schedule(cls, subject_id, thedate):
        if isinstance(thedate, str):
            if not util.test_date(thedate):
                raise InvalidValueError("Invalid date format")
            dt = thedate
        elif hasattr(thedate, 'strftime'):
            dt = thedate.strftime("%Y-%m-%d")
        else:
            raise InvalidValueError("Invalid date format")

        if cls.select().where((cls.subject == subject_id) & (cls.date == dt)).exists():
            return True

        return cls.create(subject=subject_id, date=dt)

    @classmethod
    def unschedule(cls, subject_id, thedate):
        if isinstance(thedate, str):
            if not util.test_date(thedate):
                raise InvalidValueError("Invalid date format")
            dt = thedate
        elif hasattr(thedate, 'strftime'):
            dt = thedate.strftime("%Y-%m-%d")
        else:
            raise InvalidValueError("Invalid date format")

        if cls.select().where((cls.subject == subject_id) & (cls.date == dt)).exists():
            return bool(cls.delete().where((cls.subject == subject_id) & (cls.date == dt)).execute())
        else:
            return True

    @classmethod
    def fetch_single_date(cls, thedate=None):
        dt = datetime.today()

        if hasattr(thedate, 'strftime'):
            dt = thedate.strftime("%Y-%m-%d")
        elif isinstance(thedate, str):
            if not util.test_date(thedate):
                raise InvalidValueError("Invalid date_from date format")
            dt = thedate

        subject_model = cls.subject.rel_model
        query = subject_model.select().join(cls, on=cls.subject).where(cls.date == dt)

        return list(query)

    @classmethod
    def fetch_range(cls, date_from=None, date_to=None):
        dfrom = date.today()
        dto = date.today() + timedelta(days=1)

        if hasattr(date_from, 'strftime'):
            dfrom = date_from.strftime("%Y-%m-%d")
        elif isinstance(date_from, str):
            if not util.test_date(date_from):
                raise InvalidValueError("Invalid date_from date format")
            dfrom = date_from

        if hasattr(date_from, 'strftime'):
            dto = date_to.strftime("%Y-%m-%d")
        elif isinstance(date_to, str):
            if not util.test_date(date_to):
                raise InvalidValueError("Invalid date_to date format")
            dto = date_to

        if dfrom > dto:
            raise InvalidValueError("date_to must be greater than date_from")

        query = cls.select().where((cls.date >= dfrom) & (cls.date <= dto))

        return list(query)
