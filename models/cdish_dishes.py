from ._base import *
from .dish import Dish
from .cdish import CDish


class CDishDishes(BaseModel):
    cdish = ForeignKeyField(CDish)
    dish = ForeignKeyField(Dish)
    group = CharField(default='')

    class Meta:
        db_table = 'cdishes_dishes'

    @classmethod
    def link(cls, cdish_id, dish_id, group=0):
        if cls.select().where((cls.cdish == cdish_id) & (cls.dish == dish_id) & (cls.group == group)).exists():
            return True
        return cls.create(cdish=cdish_id, dish=dish_id, group=group)

    @classmethod
    def unlink(cls, cdish_id, dish_id):
        return cls.delete().where((cls.cdish == cdish_id) & (cls.dish == dish_id)).execute()
