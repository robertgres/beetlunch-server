from ._base import *
from .dish_type import DishType


class Dish(BaseModel):
    name = CharField(index=True)
    price = DecimalField(10, 2, default=0)
    weight = IntegerField(null=True)
    type = ForeignKeyField(DishType, null=True)

    class Meta:
        db_table = 'dishes'

    def set_name(self, name: str):
        rules = re.compile(r'[\w\d\-_\s\'.]+')
        if not rules.match(name):
            raise InvalidValueError("Name is invalid")
        self.name = name
        self.save()
        return self.name == name

    def set_price(self, price: float):
        pr = round(float(price), 2)
        if pr < 0:
            raise InvalidValueError("Price can't be less than 0")
        self.price = pr
        self.save()
        return self.price == pr

    def set_weight(self, weight: int):
        if int(weight) < 0:
            raise InvalidValueError("Weight can't be less than 0. At least in our universe")
        self.weight = weight
        self.save()
        return self.weight == int(weight)

    def set_type(self, dtype):
        if isinstance(dtype, int) or isinstance(dtype, str):
            dt = DishType.handle_get(id=int(dtype))
        elif isinstance(dtype, DishType):
            dt = dtype
        else:
            raise InvalidValueError("Invalid value for dish type")
        self.type = dt.id
        self.save()
        return self.type == dt
