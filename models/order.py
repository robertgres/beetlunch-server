from ._base import *
from .user import User
from .dish import Dish
from .cdish import CDish
from datetime import datetime, timedelta
import json
import re


class OrderData:
    def __init__(self, dishes, cdishes):
        self.dishes = dishes or []
        self.cdishes = cdishes or []

    @classmethod
    def from_json(cls, text: str):
        info = json.loads(text)
        return cls(info['dishes'], info['cdishes'])

    @classmethod
    def from_ids(cls, dish_ids, cdish_ids):
        dishes = list(Dish.select().where(Dish.id << dish_ids))
        cdishes = list(CDish.select().where(CDish.id << cdish_ids))
        return cls(dishes, cdishes)

    def collect_data(self):
        return dict(
            dishes=json_safe_data(self.dishes),
            cdishes=json_safe_data(self.cdishes)
        )

    def to_json(self):
        return json.dumps(self.collect_data())


class Order(BaseModel):
    created_on = DateTimeField(null=False, default=datetime.utcnow())
    last_modified = DateTimeField(null=False, default=datetime.utcnow())
    date = DateField(null=False)
    user = ForeignKeyField(User)
    status = IntegerField(choices=[0, 1, 2, 3], default=0, null=False)
    paid = BooleanField(default=0)
    data = TextField(null=False)

    ENUM_STATUS = {0: 'placed', 1: 'in_process', 2: 'delivered', 3: 'cancelled'}

    class Meta:
        db_table = 'orders'
        order_by = ('date',)

    @classmethod
    def status_name(cls, code):
        if isinstance(code, str) and code in cls.ENUM_STATUS.values():
            return code
        elif int(code) in cls.ENUM_STATUS.keys():
            return cls.ENUM_STATUS.get(int(code))
        raise ValueError("Status with code %s doesn't exist" % code)

    @classmethod
    def status_code(cls, name):
        if isinstance(name, str):
            for code, cname in cls.ENUM_STATUS:
                if cname == name:
                    return code
        elif int(name) in cls.ENUM_STATUS.keys():
            return int(name)
        raise ValueError("Status with name %s doesn't exist" % name)

    @classmethod
    def place(cls, user_id: int, order_data: OrderData, for_date: datetime=None):
        if not User.select().where(User.id == user_id).exists():
            raise EntityDoesNotExistError("User doesn't exist")
        dt = for_date or datetime.now().strftime("%Y-%m-%d")
        return cls.create(date=dt, user=int(user_id), data=order_data.to_json())

    @classmethod
    def fetch_list(cls, limit: int = 10, offset: int = 0, user_ids: list=None):
        query = cls.select().limit(limit).offset(offset)
        if user_ids:
            query = query.where(cls.user << user_ids)
        return list(query)

    @classmethod
    def fetch_range(cls, date_from=None, date_to=None, user_ids=None):
        dfrom = datetime.today()
        dto = datetime.today() + timedelta(days=1)
        strrule = re.compile(r'^\d{4}-\d{2}-\d{2}$')

        if hasattr(date_from, 'strftime'):
            dfrom = date_from.strftime("%Y-%m-%d")
        elif isinstance(date_from, str):
            if not strrule.match(date_from):
                raise InvalidValueError("Invalid date_from date format")
            dfrom = date_from

        if hasattr(date_from, 'strftime'):
            dto = date_to.strftime("%Y-%m-%d")
        elif isinstance(date_to, str):
            if not strrule.match(date_to):
                raise InvalidValueError("Invalid date_to date format")
            dto = date_to

        if dfrom > dto:
            raise InvalidValueError("date_to must be greater than date_from")

        query = cls.select().where((cls.date >= dfrom) & (cls.date <= dto))
        if user_ids:
            query = query.where(cls.user << user_ids)

        return list(query)

    def get_data(self):
        return OrderData.from_json(self.data)

    def collect_data(self):
        data = self.serialize()
        data['data'] = json_safe_data(self.get_data())
        data['user'] = self.user.collect_data()
        return data

    def modify(self, order_data: OrderData):
        if self.status > 0:
            raise InvalidOperationError("Cannot modify order with status: %s" % self.status_name(self.status))
        self.data = order_data.to_json()
        self.last_modified = datetime.utcnow()
        return bool(self.save())

    def cancel(self):
        if self.status > 0:
            raise InvalidOperationError("Cannot cancel order with status: %s" % self.status_name(self.status))
        self.status = 4
        return bool(self.save())
