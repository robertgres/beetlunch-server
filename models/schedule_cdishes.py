from ._base import *
from .cdish import CDish


class ScheduleCDishes(SchedulerModel):
    subject = ForeignKeyField(CDish)

    class Meta:
        db_table = 'schedule_cdishes'
