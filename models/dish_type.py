from ._base import *
import re


class DishType(BaseModel):
    name = CharField(null=False, unique=True)

    class Meta:
        db_table = 'dish_types'

    @classmethod
    def create_one(cls, name: str):
        rules = re.compile(r'[\w\d\-_\s]+')
        if not rules.match(name):
            raise InvalidValueError("Name is invalid")
        return cls.create(name=name)

    def set_name(self, name):
        rules = re.compile(r'[\w\d\-_\s]+')
        if not rules.match(name):
            raise InvalidValueError("Name is invalid")
        self.name = name
        self.save()
        return self.name == name
