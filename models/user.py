from ._base import *
from datetime import datetime, timedelta
import jwt


class User(BaseModel):
    email = CharField(null=False, unique=True, index=True)
    password = CharField(null=False)
    access_level = IntegerField(choices=[0, 1, 2], default=1, null=False)
    registered_on = DateTimeField(default=datetime.utcnow(), null=False)
    nickname = CharField()
    first_name = CharField()
    last_name = CharField()

    ENUM_ACCESS_LEVEL = {0: 'none', 1: 'normal', 2: 'admin'}

    class Meta:
        db_table = 'users'

    @classmethod
    def access_level_code(cls, name):
        if isinstance(name, str):
            for code, n in cls.ENUM_ACCESS_LEVEL.items():
                if name == n:
                    return code
        elif int(name) in cls.ENUM_ACCESS_LEVEL.keys():
            return int(name)
        raise ValueError('Access level with name "%s" not found' % name)

    @classmethod
    def access_level_name(cls, code):
        if isinstance(code, str):
            if code in cls.ENUM_ACCESS_LEVEL.values():
                return code
            else:
                raise ValueError('Access level with code "%s" not found' % code)
        elif int(code) in cls.ENUM_ACCESS_LEVEL.keys():
            return cls.ENUM_ACCESS_LEVEL[int(code)]
        raise ValueError('Access level with code "%s" not found' % code)

    @classmethod
    def register(cls, email: str, password: str):
        email_regexpr = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        if not email_regexpr.match(email):
            raise InvalidValueError('Invalid email')
        if len(password) != 64:
            raise InvalidValueError('Password must be hashed as SHA-256')

        query = cls.select().where(cls.email == email)
        if query.exists():
            raise EntityAlreadyExistError("User with email %s already exists" % email)

        user = cls.create(email=email, password=password)
        return user

    def collect_data(self):
        data = self.serialize()
        del data['password']
        return data

    def is_admin(self):
        return self.access_level > 1

    def has_access_level(self, access_level):
        return self.access_level >= self.access_level_code(access_level)

    def deactivate(self):
        self.set_access_level(0)
        self.save()
        return self.access_level == 0

    def set_access_level(self, level: int):
        code = User.access_level_code(level)
        self.access_level = code
        self.save()
        return self.access_level == code

    def set_nickname(self, nickname: str):
        rules = re.compile(r'^[\w\d_]+$')
        if not rules.match(nickname):
            raise InvalidValueError("Wrong characters used in nickname")
        self.nickname = nickname
        self.save()
        return self.nickname == nickname

    def set_first_name(self, first_name: str):
        rules = re.compile(r'^[\w\d\-_\s\'.]+$')
        if not rules.match(first_name):
            raise InvalidValueError("Wrong characters used in first_name")
        self.first_name = first_name
        self.save()
        return self.first_name == first_name

    def set_last_name(self, last_name: str):
        rules = re.compile(r'^[\w\d\-_\s\'.]+$')
        if not rules.match(last_name):
            raise InvalidValueError("Wrong characters used in last_name")
        self.last_name = last_name
        self.save()
        return self.last_name == last_name

    def set_password(self, new_pass: str):
        if len(new_pass) != 64:
            raise InvalidValueError('Password must be hashed as SHA-256')
        self.password = new_pass
        self.save()
        return self.password == new_pass

    def set_email(self, new_email: str):
        email_regexpr = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        if not email_regexpr.match(new_email):
            raise InvalidValueError('Invalid email')
        if User.select().where(User.email == new_email).exists():
            raise EntityAlreadyExistError("User with email %s already exists" % new_email)
        self.email = new_email
        self.save()
        return self.email == new_email
