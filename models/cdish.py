from ._base import *
from .dish import Dish
import re


class CDish(BaseModel):
    name = CharField()
    price = DecimalField(10, 2, default=0)

    class Meta:
        db_table = 'cdishes'

    def collect_data(self):
        data = self.serialize()
        CDishDishes = self.lookup_model('CDishDishes')
        links = CDishDishes.select().where(CDishDishes.cdish == self.get_id())
        groups = {}
        for link in links:
            if link.group not in groups:
                groups[link.group] = []
            groups[link.group].append(json_safe_data(link.dish))
        data['components'] = groups
        return data

    def set_name(self, name: str):
        rules = re.compile(r'[\w\d\-_\s\'.]+')
        if not rules.match(name):
            raise InvalidValueError("Name is invalid")
        self.name = name
        self.save()
        return self.name == name

    def set_price(self, price: float):
        pr = round(float(price), 2)
        if pr < 0:
            raise InvalidValueError("Price can't be less than 0")
        self.price = pr
        self.save()
        return self.price == pr
