from ._base import *
from .dish import Dish
from datetime import datetime
import re


class ScheduleDishes(SchedulerModel):
    subject = ForeignKeyField(Dish)

    class Meta:
        db_table = 'schedule_dishes'
