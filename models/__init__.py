from errors import *
from ._base import *
from .dish_type import *
from .dish import *
from .cdish import *
from .cdish_dishes import *
from .order import *
from .schedule_cdishes import *
from .schedule_dishes import *
from .user import *


def list_models():
    return [
        DishType,
        Dish,
        CDish,
        CDishDishes,
        ScheduleDishes,
        ScheduleCDishes,
        User,
        Order,
    ]
