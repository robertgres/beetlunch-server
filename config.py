import os

env = os.environ.get('ENV', None)
auth_secret_key = os.environ.get('AUTH_SECRET_KEY', None)
token_expiration_delta = dict(days=1, minutes=30)
debug = bool(os.environ.get('DEBUG', True))
max_days_in_response = 365
max_weeks_in_response = 42

allowed_origins = os.environ.get('ALLOWED_ORIGINS', '').split(',')

database = dict(
    database=os.environ.get('DB_DATABASE', None),
    host=os.environ.get('DB_HOST', None),
    user=os.environ.get('DB_USER', None),
    passwd=os.environ.get('DB_PASSWORD', None)
)

recaptcha = dict(
    enable=True,
    secret_key=os.environ.get('RECAPTCHA_SECRET_KEY', None),
    public_key=os.environ.get('RECAPTCHA_PUBLIC_KEY', None),
    endpoint='https://www.google.com/recaptcha/api/siteverify'
)
